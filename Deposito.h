/* 
 * File:   Deposito.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:46
 */

#ifndef DEPOSITO_H
#define	DEPOSITO_H

template<class T>
class Deposito {
    private:
        int idDeposito; //chave de um Depósito
        int capacidadeMaxima; //capaxidade máxima das paletes
        int limitePaletes; //limite de paletes do Depósito (N paletes)
        float area; //área total ocupada 
       
    public:
        Deposito(int id, int capacidade, int limite, float a);
        Deposito();
        Deposito(const Deposito<T>& d);
        virtual ~Deposito();
        
        int getIDDeposito() const;
        int getCapacidadeMaxima() const;
        int getLimitePaletes() const;
        float getArea() const;
        
        void setIDDeposito(int id);
        void setCapacidadeMaxima(int capacidade);
        void setArea(float a);
        void setLimitePaletes(int limite);
    
        virtual bool inserirProdutos(const vector<T *>& vprd) = 0;
        virtual vector<T> expedirProdutos(int qtd) = 0;
        virtual bool isFull() const = 0;
        
        virtual bool operator==(const Deposito<T>& d) const;
        virtual Deposito<T>& operator=(const Deposito<T>& d);
        
        virtual void escreve(ostream& out) const;
        virtual void writeFile(ostream& out) const;
        virtual Deposito* clone() const = 0;
};

template<class T>
Deposito<T>::Deposito(int id, int capacidade, int limite, float a) {
    idDeposito = id;
    limitePaletes = limite;
    capacidadeMaxima = capacidade;
    area = a;
}

template<class T>
Deposito<T>::Deposito() {
    idDeposito = 0;
    limitePaletes = 0;
    capacidadeMaxima = 0;
    area = 0;
}

template<class T>
Deposito<T>::Deposito(const Deposito<T>& d) {
    idDeposito = d.idDeposito;
    limitePaletes = d.limitePaletes;
    capacidadeMaxima = d.capacidadeMaxima;
    area = d.area;
}

template<class T>
Deposito<T>::~Deposito() {
    
}

template<class T>
int Deposito<T>::getIDDeposito() const {
    return idDeposito;
}

template<class T>
int Deposito<T>::getCapacidadeMaxima() const {
    return capacidadeMaxima;
}

template<class T>
int Deposito<T>::getLimitePaletes() const {
    return limitePaletes;
}

template<class T>
float Deposito<T>::getArea() const {
    return area;
}

template<class T>
void Deposito<T>::setIDDeposito(int id) {
    idDeposito = id;
}

template<class T>
void Deposito<T>::setLimitePaletes(int limite) {
    limitePaletes = limite;
}

template<class T>
void Deposito<T>::setCapacidadeMaxima(int capacidade) {
    capacidadeMaxima = capacidade;
}

template<class T>
void Deposito<T>::setArea(float a) {
    area = a;
}

template<class T>
bool Deposito<T>::operator==(const Deposito<T>& d) const {
    if(this->idDeposito == d.idDeposito && this->area == d.area && this->capacidadeMaxima == d.capacidadeMaxima && this->limitePaletes == d.limitePaletes) {
        return true;
    }
    return false;
}

template<class T>
Deposito<T>& Deposito<T>::operator=(const Deposito<T>& d) {
    if(this != &d) {
        idDeposito = d.idDeposito;
        area = d.area;
        capacidadeMaxima = d.capacidadeMaxima;
        limitePaletes = d.limitePaletes;
    }
    
    return *this;
}

template<class T>
void Deposito<T>::escreve(ostream& out) const {
    out << " ID: " << idDeposito << " (Capacidade Máxima Paletes: " << capacidadeMaxima << "; Área: " << area << "; Limite Paletes: " << limitePaletes << ")" << endl;
}

template<class T>
void Deposito<T>::writeFile(ostream& out) const {
    out << idDeposito << ";" << capacidadeMaxima << ";" << limitePaletes << ";" << area; // escreve o IDDeposito; capacidadeMaxima; limitePaletes; area 
}

template<class T>
ostream& operator <<(ostream& out, const Deposito<T>& d) {
    d.escreve(out);
    return out;
}

#endif	/* DEPOSITO_H */

