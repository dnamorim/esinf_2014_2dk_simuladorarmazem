//
//  Armazem.h
//  ESINF_SimuladorArmazem
//
//  Created by Duarte Nuno Amorim on 04/12/14.
//  Copyright (©) 2014 Duarte Nuno Amorim. All rights reserved.
//

#ifndef ESINF_SimuladorArmazem_Armazem_h
#define ESINF_SimuladorArmazem_Armazem_h

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <queue>
#include <stack>

#include "graphStlPath.h"
#include "Deposito.h"
#include "Produto.h"

class Armazem : public graphStlPath<Deposito<Produto> *, double> {
private:
    int ID;
    map<int, Deposito<Produto> *> lstDep;

    queue<stack<Deposito<Produto>*>> percursosPossiveisDepositos(Deposito<Produto>* &di, Deposito<Produto>* &df);
    void menorPercurso(Deposito<Produto>* &dpA, Deposito<Produto>* &dpB);
    void mostraCaminho(int vi, const vector<int> &path);

public:
    Armazem(int nr);
    Armazem();
    ~Armazem();

    int getID() const;
    void setID(int nr);

    bool readFile(string File);
    void percursosPossiveisDepositos(int idA, int idB);
    void showPaths(stack<Deposito<Produto>*> stk);
    void percursoTipo(int idA, int idB);
    void menorPercursoDepositos(int idA, int idB);

};

Armazem::Armazem(int nr) {
    ID = nr;
}

Armazem::Armazem() {
    ID = 0;
}

Armazem::~Armazem() {

}

int Armazem::getID() const {
    return ID;
}

void Armazem::setID(int nr) {
    ID = nr;
}

bool Armazem::readFile(string path) {
    ifstream armFile;
    string line;
    int ini = 0, pos;

    // Abrir o ifstream
    armFile.open(path);

    if (!armFile) {
        cout << "Erro: O Ficheiro fornecido não existe." << endl;
        return false;
    }

    // Obter ID do Armazém
    std::getline(armFile, line);

    if(line.size() > 0) {
        ID = stoi(line);
    }

    //  Obter tamanho Matriz
    std::getline(armFile, line);

    pos = (int) line.find(";", 0);
    int nl = stoi(line.substr(ini, pos));
    int nc = stoi(line.substr(pos+1, line.find(";", pos+1) - pos));

    double** distancias;
    //  Criar Matriz de Distâncias
    distancias = new double*[nl];

    for (int i = 0; i < nl; ++i) {
        distancias[i] = new double[nc];
    }

    //  Preencher Matriz de Distâncias via Ficheiro
    for (int i = 0; i < nl; ++i) {
        std::getline(armFile, line); // Obter Linha de Ficheiro
        ini = 0;
        for (int j = 0; j < nl; ++j) {
            pos = (int) line.find(";", ini);
            distancias[i][j] = stod(line.substr(ini, pos));
            pos++;
            ini = pos;
        }
    }

    //  Obter Depósitos: em nl está o número de depósitos
    int id, capmax, limpal;
    float area;
    string tipo;


    //  Ler Produtos nos Depósitos
    for (int i=0; i < nl; ++i) {
        std::getline(armFile, line);
        std::getline(armFile, line);

        //  ID Depósito
        ini = 0;
        pos = (int) line.find(";", ini);
        id = stoi(line.substr(ini, pos-ini));

        //  Capacidade Máxima do Depósito
        pos++;
        ini = pos;
        pos = (int) line.find(";", ini);
        capmax = stoi(line.substr(ini, pos-ini));

        //  Limite de Paletes do Depósito
        pos++;
        ini = pos;
        pos = (int) line.find(";", ini);
        limpal = stoi(line.substr(ini, pos-ini));

        //  Área do Depósito
        pos++;
        ini = pos;
        pos = (int) line.find(";", ini);
        area = stof(line.substr(ini, pos-ini));

        //  Tipo do Depósito
        pos++;
        ini = pos;
        pos = (int) line.find(";", ini);
        tipo = line.substr(ini, pos-ini);

        //  Criar o Depósito
        if(tipo == "Fresco") {
            DepositoFresco<Produto> df(id, capmax, limpal, area);

            //  Ler Produtos e Inserir no depósito Fresco
            vector<PaleteFresco<Produto>> pf = df.getDeposito();
            for (int i = 0; i < limpal; i++) {
                std::getline(armFile, line);

                ini = 0;
                pos = (int) line.find(";", ini);
                while(pos != -1) {
                    pf[i].push(ProdutoFresco(line.substr(ini, pos-ini)));
                    pos++;
                    ini = pos;
                    pos = (int) line.find(";", ini);
                }
            }

            df.setDeposito(pf);
            lstDep[id] = df.clone();

        } else {
            DepositoNormal<Produto> dn(id, capmax, area , limpal);

            //  Ler Produtos e Inserir no depósito Normal
            vector<PaleteNormal<Produto>> pn = dn.getDeposito();
            for (int i = 0; i < limpal; i++) {
                std::getline(armFile, line);

                ini = 0;
                pos = (int) line.find(";", ini);
                while(pos != -1) {
                    pn[i].push(ProdutoNormal(line.substr(ini, pos-ini)));
                    pos++;
                    ini = pos;
                    pos = (int) line.find(";", ini);
                }
            }

            dn.setDeposito(pn);
            lstDep[id] = dn.clone();
        }
    }

    armFile.close();

    //  Construir Grafo
    for(int i=0; i < nl; i++) {
        for (int j=0; j < nc; j++) {
            if(distancias[i][j] != 0) {
                this->addGraphEdge(distancias[i][j], lstDep.find(i)->second, lstDep.find(j)->second);
            }
        }
    }

    return true;
}

void Armazem::percursosPossiveisDepositos(int idA, int idB) {
    if(lstDep.find(idA) == lstDep.end() || lstDep.find(idB) == lstDep.end()) {
        cout << "ERRO: Um ou mais ID de depósitos fornecidos não existe no ficheiro importado." << endl;
        return ;
    }

    cout << "Percursos Possíveis entre Depósitos: " << endl;

    queue < stack<Deposito<Produto>* > > dstPaths;
    queue < stack<Deposito<Produto>* > > queAux;

    dstPaths = this->distinctPaths(lstDep.find(idA)->second, lstDep.find(idB)->second);

    for (queAux = dstPaths; !queAux.empty(); queAux.pop()) {
        showPaths(queAux.front());
    }
    cout << endl;
}

void Armazem::showPaths(stack<Deposito<Produto>* > stk) {

    vector<Deposito<Produto> * > vdep;

    while (!stk.empty()) {
        vdep.push_back(stk.top());
        stk.pop();
    }

    double ramo;

    cout << "Dep. ID " << vdep[vdep.size() - 1]->getIDDeposito();

    for (long i = vdep.size(); i > 1; i--) {
        this->getEdgeByVertexContents(ramo, vdep[i - 1], vdep[i - 2]);
        cout << " -> " << ramo << "m -> Dep. ID " << vdep[i - 2]->getIDDeposito();
    }

    cout << endl;

}

void Armazem::menorPercursoDepositos(int idA, int idB) {
    if(lstDep.find(idA) == lstDep.end() || lstDep.find(idB) == lstDep.end()) {
        cout << "ERRO: Um ou mais ID de depósitos fornecidos não existe no ficheiro importado." << endl;
    } else {
        cout << "Menor distância entre " << idA << " e " << idB << ": ";
        menorPercurso(lstDep.find(idA)->second, lstDep.find(idB)->second);
    }
    cout << "\n" << endl;
}

void Armazem::menorPercurso(Deposito<Produto>* &dpA, Deposito<Produto>* &dpB) {
    vector <int> path;
    vector<double> dist;
    int key;

    this->getVertexKeyByContent(key, dpB);
    this->dijkstrasAlgorithm(dpA, path, dist);
    cout << dist[key] << "m -  Percurso:  " << endl;
    cout << "Dep. ID " << (*dpA).getIDDeposito();
    mostraCaminho(key, path);
}

void Armazem::mostraCaminho(int vi, const vector<int> &path) {
    if (path[vi] == -1) {
        return;
    }
    mostraCaminho(path[vi], path);
    double e;
    this->getEdgeByVertexKeys(e, path[vi], vi);

    Deposito<Produto>* c;
    this->getVertexContentByKey(c, vi);
    cout << " -> " << e << "m -> Dep. ID " << (*c).getIDDeposito();
}


void Armazem::percursoTipo(int idA, int idB) {

    if(lstDep.find(idA) == lstDep.end() || lstDep.find(idB) == lstDep.end()) {
        cout << "ERRO: Um ou mais ID de depósitos fornecidos não existe no ficheiro importado." << endl;
        return ;
    } else {
        if (typeid (*lstDep.find(idA)->second) != typeid (*lstDep.find(idB)->second)) {
            cout << "ERRO: O Depósito Inicial e final não são do mesmo tipo." << endl;
            return;
        }
    }
    cout << "Percurso em Depósitos do mesmo tipo: " << endl;

    Deposito<Produto>* dpO = lstDep.find(idA)->second;
    Deposito<Produto>* dpD = lstDep.find(idB)->second;

    queue < stack<Deposito<Produto>* > > distPaths;
    queue < stack<Deposito<Produto>* > > que;
    stack <Deposito<Produto>* > stk;
    bool sametype;

    distPaths = this->distinctPaths(dpO, dpD);

    for (que = distPaths; !que.empty(); que.pop()) {
        stk = que.front();

        sametype = true;

        while (!stk.empty()) {
            Deposito<Produto>* dep = stk.top();

            if (typeid (dep) != typeid (dpO)) {
                sametype = false;
                break;
            }
            stk.pop();
        }

        if (sametype) {
            showPaths(que.front());
            return;
        }

    }

}




#endif
