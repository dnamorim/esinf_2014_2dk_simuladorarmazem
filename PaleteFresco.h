/* 
 * File:   Palete.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:47
 */

#ifndef PALETEFRESCO_H
#define	PALETEFRESCO_H

template<class T>
class PaleteFresco {

    private :
        queue<T> palete;
        int capacidade;
        
    public :
        PaleteFresco(int c);
        PaleteFresco(const PaleteFresco<T>& pf);
        PaleteFresco();
        ~PaleteFresco();
        
        PaleteFresco<T>* clone() const;
        
        bool push(const T& p);
        T front();
        void pop();
        int size();
        
        bool isFull() const;
        int getCapacidade() const;
        
        bool operator==(const PaleteFresco<T>& pf);
        PaleteFresco<T>& operator=(const PaleteFresco<T>& pf);
        
        void escreve(ostream& out) const;
        void writeFile(ostream& out) const;
};

template<class T>
PaleteFresco<T>::PaleteFresco(int c) : palete() {
    capacidade = c;
}

template<class T>
PaleteFresco<T>::PaleteFresco(const PaleteFresco& pf) : palete(pf.palete) {
    capacidade = pf.capacidade;
}

template<class T>
PaleteFresco<T>::PaleteFresco() {
    palete();
    capacidade = 0;
}

template<class T>
PaleteFresco<T>::~PaleteFresco() {
    
}

template<class T>
int PaleteFresco<T>::getCapacidade() const {
    return capacidade;
}

template<class T>
bool PaleteFresco<T>::push(const T& p) {
    if(palete.size() < capacidade) {
        palete.push(p);
        return true;
    }
    return false;
}

template<class T>
T PaleteFresco<T>::front() {
    return palete.front();
}

template<class T>
void PaleteFresco<T>::pop() {
    palete.pop();
}

template<class T>
bool PaleteFresco<T>::isFull() const {
    if(palete.size() == capacidade) {
        return true;
    } else {
        return false;
    }
}

template<class T>
int PaleteFresco<T>::size() {
    return (int) palete.size();
}

template<class T>
bool PaleteFresco<T>::operator ==(const PaleteFresco<T>& pf) {
    //se o tamanho das paletes forem diferentes
    if((int) palete.size() == (int) pf.palete.size()) {
        queue<T> que1 = palete;
        queue<T> que2 = pf.palete;
        
        while(!que1.empty()) {
            T t1 = que1.front();
            T t2 = que2.front();
            
            //se os objectos actuais da Paletes forem diferentes
            if(t1 != t2) {
                return false;
            }
            que1.pop();
            que2.pop();
        }
        
        return true;
    }
    
    return false;
}

template<class T>
PaleteFresco<T>& PaleteFresco<T>::operator =(const PaleteFresco<T>& pf) {
    if(this != &pf) {
        queue<T> que = pf.palete;
        palete = que;
        capacidade = pf.capacidade;
    }
    return *this;
}

template<class T>
PaleteFresco<T>* PaleteFresco<T>::clone() const {
    return new PaleteFresco(*this);
}

template<class T>
void PaleteFresco<T>::escreve(ostream& out) const{
    queue<T> que = palete;
    while(!que.empty()) {
        out << que.front() << "; ";
        que.pop();
    }
    
    out << endl;
    
}

template<class T>
void PaleteFresco<T>::writeFile(ostream& out) const{
    queue<T> que = palete;
    while(!que.empty()) {
        que.front().writeFile(out);
        out << ";";
        que.pop();
    }
    
    out << endl;
    
}

template <typename T>
ostream& operator << (ostream& out, const PaleteFresco<T>& pf) {
    pf.escreve(out);
    return out;
}

#endif	/* PALETENORMAL_H */

