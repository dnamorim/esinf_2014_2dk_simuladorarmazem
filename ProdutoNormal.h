/* 
 * File:   ProdutoNormal.h
 * Author: Ricardo Rodrigues
 *
 * Created on 22 de Outubro de 2014, 22:39
 */

#ifndef PRODUTONORMAL_H
#define	PRODUTONORMAL_H

class ProdutoNormal : public Produto
{
    public :
        ProdutoNormal(const string& nome);
        ProdutoNormal();
        ProdutoNormal(const ProdutoNormal& p);
        ~ProdutoNormal();
        Produto* clone() const;
        
        bool operator==(const ProdutoNormal& pn);
        ProdutoNormal& operator=(const ProdutoNormal& pn);
        
        void escreve(ostream& out) const;
};

ProdutoNormal::ProdutoNormal(const string& nome) : Produto(nome) {
    
}

ProdutoNormal::ProdutoNormal(const ProdutoNormal& pn) : Produto(pn) {
    
}

ProdutoNormal::~ProdutoNormal() {
    
}

Produto* ProdutoNormal::clone() const {
    return new ProdutoNormal(*this);
}

bool ProdutoNormal::operator ==(const ProdutoNormal& pn) {
    if (typeid(pn) == typeid(ProdutoNormal)) {
        return Produto::operator ==(pn);
    }
    
    return false;
}

ProdutoNormal& ProdutoNormal::operator =(const ProdutoNormal& pn) {
    if(this != &pn) {
        Produto::operator =(pn);
    }
    
    return *this;
}

void ProdutoNormal::escreve(ostream& out) const {
    Produto::escreve(out);
    out << "(Tipo: Produto Normal)" << endl;
}
        
ostream& operator<<(ostream& out, const ProdutoNormal& pn) {
    pn.escreve(out);
    return out;
}

#endif	/* PRODUTONORMAL_H */

