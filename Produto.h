/* 
 * File:   Produto.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:47
 */

#ifndef PRODUTO_H
#define	PRODUTO_H

class Produto {
    private: 
        string nomeProduto;
        
    public:
        Produto();
        Produto(string nomeProd);
        Produto(const Produto & p);
        virtual ~Produto();
        
        string getNomeProduto() const;
        void setNomeProduto(const string nomeProd);

        virtual bool operator == (const Produto& pr) const;
        virtual Produto& operator = (const Produto& pr);
        
        virtual Produto* clone() const;
        virtual void escreve(ostream& out) const;
        virtual void writeFile(ostream& out) const;
};

Produto :: Produto(){
    nomeProduto = "";
}

Produto :: Produto(string nomeProd) {
    nomeProduto = nomeProd;
}

Produto :: Produto(const Produto & p){
    nomeProduto = p.nomeProduto;
}

Produto :: ~Produto(){
    
}

string Produto:: getNomeProduto() const {
	return nomeProduto;
}

void Produto:: setNomeProduto(string n) {
    nomeProduto = n;
}

Produto& Produto :: operator = (const Produto& p){
    if(this!=&p){
        nomeProduto = p.nomeProduto;
    }
    
    return *this;
}

bool Produto:: operator == (const Produto& p) const{
    if(nomeProduto == p.nomeProduto) {
        return true;
    }
    
    return false;
}

Produto* Produto:: clone() const{
    return new Produto(*this);
}

void Produto::escreve(ostream& out) const {
    out << nomeProduto << " ";
}

void Produto::writeFile(ostream& out) const {
    out << nomeProduto;
}


ostream& operator << (ostream& out, const Produto& p) {
    p.escreve(out);
    return out;
}


#endif	/* PRODUTO_H */

