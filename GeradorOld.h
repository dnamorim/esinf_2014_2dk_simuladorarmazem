/*
 * File:   GeradorOld.h
 * Author: dnamorim
 *
 * Created on 25 de Outubro de 2014, 17:41
 */

#ifndef GeradorOld_H
#define	GeradorOld_H

#include <stdlib.h>  /* Functions for srand, rand */
#include <time.h>

#include "ArmazemOld.h"
#include "DepositoFresco.h"
#include "DepositoNormal.h"

template<class T>
class GeradorOld {
    private:
        int min;
        int max;
        int nArm;
        ArmazemOld<T> arm;

        int gerarInt(int min, int max);
        float gerarFloat(int min, int max);
        void startRand();
        void gerarDistancias(int n);
        void criarDepositosFrescos(int n);
        void criarDepositosNormais(int n);

    public :
        GeradorOld(int i, int j);
        GeradorOld(const GeradorOld& g);
        GeradorOld();
        ~GeradorOld();

        void inserirProdutos(const vector<T *>& vprd);
        void expedirProdutos();

        void mostraArmazemOld();
        void gravarArmazemOld(string str);
};

template<class T>
void GeradorOld<T>::startRand() {
    srand (time(NULL));
}

template<class T>
int GeradorOld<T>::gerarInt(int min, int max) {
    return rand() % max + min;
}

template<class T>
float GeradorOld<T>::gerarFloat(int min, int max) {
    return rand() % max + min;
}

template<class T>
GeradorOld<T>::GeradorOld(int i, int j) {
    this->min = i;
    while(min <= 0) {
        cout << "Defina um novo valor Mínimo: ";
        cin >> min;
    }

    this->max = j;
    while(max <= 0 || max <= min) {
        cout << "Defina um novo valor Máximo: ";
        cin >> max;
    }

    int nrDepositosFrescos = gerarInt(min, max);
    int nrDepositosNormais = gerarInt(min, max);
    nArm = nrDepositosFrescos+nrDepositosNormais;
    int id = gerarInt(1, max);

    arm = ArmazemOld<T>(id, nrDepositosFrescos , nrDepositosNormais);
    criarDepositosFrescos(nrDepositosFrescos);
    criarDepositosNormais(nrDepositosNormais);
    gerarDistancias(nrDepositosFrescos+nrDepositosNormais);
}

template<class T>
GeradorOld<T>::GeradorOld(const GeradorOld<T>& g) {
    this->min = g.min;
    this->max = g.max;
    arm(g.arm);
}

template<class T>
GeradorOld<T>::GeradorOld() {
    this->min = 1;
    this->max = 20;
}

template<class T>
GeradorOld<T>::~GeradorOld() {

}

template<class T>
void GeradorOld<T>::criarDepositosFrescos(int n) {
    int capacidade, limitePaletes;
    float area;

    vector<DepositoFresco<T>> v;
    for (int i = 0; i < n; i++) {
        capacidade = gerarInt(min, max);
        limitePaletes = gerarInt(min, max);
        area = gerarFloat(min, max);

        arm.adicionarDepositoFresco(capacidade, limitePaletes, area);
    }
}

template<class T>
void GeradorOld<T>::criarDepositosNormais(int n) {
    int capacidade, limitePaletes;
    float area;

    for (int i = 0; i < n; i++) {

        capacidade = gerarInt(min, max);
        limitePaletes = gerarInt(min, max);
        area = gerarFloat(min, max);

        arm.adicionarDepositoNormal(capacidade, limitePaletes, area);
    }
}

template<class T>
void GeradorOld<T>::inserirProdutos(const vector<T *>& vprd) {
    if(arm.inserirProdutos(vprd)) {
        cout << "Todos os Produtos foram inseridos com sucesso no Depósito Respectivo." << endl;
    } else {
        cout << "Não foi possível adicionar todos os produtos nos Depósitos (falta de capacidade)." << endl;
    }
}

template<class T>
void GeradorOld<T>::expedirProdutos() {
    int id = gerarInt(1, nArm);
    int qtd = gerarInt(min, max);

    vector<T> exp = arm.expedirProdutos(id, qtd);
    if(exp.size() == qtd) {
        cout << "A quantidade de produtos a expedir foi satisfeita.\nProdutos Recolhidos:" << endl;
    } else {
        cout << "A quantidade de produtos a expedir não foi satisfeita (o depósito seleccionado não possuia a quantidade pedida)\nProdutos Recolhidos:" << endl;
    }

    for (int i = 0; i < exp.size(); i++) {
        cout << exp[i] << endl;
    }

    cout << endl;
}

template<class T>
void GeradorOld<T>::gerarDistancias(int n) {
    double** m = arm.getDistDepositos();
    double val = 0, ger;
    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++) {
            if(i == j) { //diagonal principal preenchida a 0's
                m[i][j] = 0;
            } else {
                // gerar distância. Utiliza-se número negativo para forçar a surgir mais depósitos sem ligação
                ger = gerarFloat(-4, max);
                val = (ger < 0) ? 0 : ger;
                m[i][j] = val;
                m[j][i] = val;
            }
        }
    }
}

template<class T>
void GeradorOld<T>::mostraArmazemOld() {
    cout << arm;
}

template<class T>
void GeradorOld<T>::gravarArmazemOld(string str) {
    arm.createFile(str);
}
#endif	/* GeradorOld_H */
