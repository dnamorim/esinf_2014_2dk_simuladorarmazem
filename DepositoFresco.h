/* 
 * File:   DepositoFresco.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:47
 */

#ifndef DEPOSITOFRESCO_H
#define	DEPOSITOFRESCO_H

#include <vector>
#include "Deposito.h"
#include "PaleteFresco.h"


template<class T>
class DepositoFresco : public Deposito<T> {
    private:
        vector<PaleteFresco<T>> deposito;
        int nextIndex; //guarda o índice da última palete onde foram adicionados produtos
        
    public:
        DepositoFresco(int id, int capacidade, int l, float a);
        DepositoFresco();
        DepositoFresco(const DepositoFresco<T>& df);
        virtual ~DepositoFresco();
        
        virtual bool inserirProdutos(const vector<T *>& vprd);
        virtual vector<T> expedirProdutos(int qtd);
        
        bool isFull() const;
        virtual vector<PaleteFresco<T>> getDeposito() const;
        virtual void setDeposito(vector<PaleteFresco<T>> vpf);
        DepositoFresco<T>& operator=(const DepositoFresco<T>& df);
        bool operator==(const DepositoFresco<T>& df);
        
        virtual void escreve(ostream& out) const;
        virtual void writeFile(ostream& out) const;
        virtual Deposito<T>* clone() const;
};

template<class T>
DepositoFresco<T>::DepositoFresco(int id, int capacidade, int l, float a) : Deposito<T>(id, capacidade, l, a) {
    for (int i = 0; i < Deposito<T>::getLimitePaletes(); i++) {
        deposito.push_back(PaleteFresco<T>(Deposito<T>::getCapacidadeMaxima()));
    }
    nextIndex = 0;
}

template<class T>
DepositoFresco<T>::DepositoFresco() {
    deposito();
    nextIndex = 0;
}

template<class T>
DepositoFresco<T>::DepositoFresco(const DepositoFresco& df) : Deposito<T>(df), deposito(df.deposito) {
    nextIndex = df.nextIndex;
}

template<class T>
DepositoFresco<T>::~DepositoFresco() {
    
}

template<class T>
Deposito<T>* DepositoFresco<T>::clone() const {
    return new DepositoFresco<T>(*this);
}

template<class T>
bool DepositoFresco<T>::inserirProdutos(const vector<T *>& vprd) {
    int i = nextIndex;
    
    if(deposito[i].isFull()) {
        return false;
    }
    
    int j, c=0;
    
    for (j = 0; j < vprd.size(); j++) {
        if(!deposito[i].isFull()) { 
            deposito[i].push(*vprd[j]);
            c++;
        }
        if(i == (Deposito<T>::getLimitePaletes()-1)) {
            i = 0;
        } else {
            i++;
        }

    }
    nextIndex = i;
    
    return (c == vprd.size() ? true : false);
}

template<class T>
vector<T> DepositoFresco<T>::expedirProdutos(int qtd) {
    vector<T> exp;
    int c=0, i=0;
    
    while(c != qtd) {
        if(deposito[i].size() != 0) {
            exp.push_back(deposito[i].front());
            deposito[i].pop();
        } else {
            break; //faz expedição de todos os produtos existentes no armazém
        }
        
        if(i == (Deposito<T>::getLimitePaletes()-1)) {
            i = 0;
        } else {
            i++;
        }
        c++;
    }
    
    return exp;
}

template<class T>
vector<PaleteFresco<T>> DepositoFresco<T>::getDeposito() const {
    return deposito;
}

template<class T>
void DepositoFresco<T>::setDeposito(vector<PaleteFresco<T>> vpf) {
    deposito = vpf;
}


template<class T>
bool DepositoFresco<T>::isFull() const {
    int flag = 0;
    
    for (int i = 0; i < deposito.size(); i++) {
        if(deposito[i].isFull()) {
            flag++;
        }
    }
    
    return ((flag == deposito.size()) ? true : false);
}

template<class T>
void DepositoFresco<T>::escreve(ostream& out) const {
    out << "Depósito Fresco";
    Deposito<T>::escreve(out);
    for (int i = 0; i < deposito.size(); i++) {
        out << "Palete Nº " << (i+1) << ": ";
        deposito[i].escreve(out);
    }
    out << endl;
}

template<class T>
void DepositoFresco<T>::writeFile(ostream& out) const {
    Deposito<T>::writeFile(out);
    out << ";Fresco" << endl;
    for (int i = 0; i < deposito.size(); i++) {
        deposito[i].writeFile(out);
    }
    out << endl;
}

template<class T>
ostream& operator <<(ostream& out, const DepositoFresco<T>& df) {
    df.escreve(out);
    return out;
}




#endif	/* DEPOSITOFRESCO_H */

