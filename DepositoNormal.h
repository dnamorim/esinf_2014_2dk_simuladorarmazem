/* 
 * File:   DepositoNormal.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:47
 */

#ifndef DEPOSITONORMAL_H
#define	DEPOSITONORMAL_H

#include <vector>
#include "Deposito.h"
#include "PaleteNormal.h"

template<class T>
class DepositoNormal : public Deposito<T> {
    private:
        vector<PaleteNormal<T>> deposito;
       
    public:
        DepositoNormal(int id, int capacidade, int l, float a);
        DepositoNormal(const DepositoNormal<T>& df);
        DepositoNormal();
        virtual ~DepositoNormal<T>();
        
        virtual bool inserirProdutos(const vector<T *>& vprd);
        virtual vector<T> expedirProdutos(int qtd);
        virtual bool isFull() const;
        virtual vector<PaleteNormal<T>> getDeposito() const;
        virtual void setDeposito(vector<PaleteNormal<T>> vpn);
    
    virtual DepositoNormal<T>& operator=(const DepositoNormal<T>& dn);
        virtual bool operator==(const DepositoNormal<T>& dn);
        
        virtual void escreve(ostream& out) const;
        virtual void writeFile(ostream& out) const;
        virtual Deposito<T>* clone() const;
};

template<class T>
DepositoNormal<T>::DepositoNormal(int id, int capacidade, int l, float a) : Deposito<T>(id, capacidade, l, a) {
    deposito.push_back(PaleteNormal<T> (Deposito<T>::getCapacidadeMaxima()));
    
    int cap = 0;
    
    for (int i = 1; i < Deposito<T>::getLimitePaletes(); i++) {
        if(i%2 == 0) {
            cap = deposito[i-1].getCapacidade()*2;
        } else {
            cap = deposito[i-1].getCapacidade()/2;
        }
        deposito.push_back(PaleteNormal<T> (cap));
    }

}

template<class T>
DepositoNormal<T>::DepositoNormal() {
    deposito();
    
}

template<class T>
DepositoNormal<T>::DepositoNormal(const DepositoNormal<T>& df) : Deposito<T>(df), deposito(df.deposito) {
    
}

template<class T>
DepositoNormal<T>::~DepositoNormal() {
    
}

template<class T>
Deposito<T>* DepositoNormal<T>::clone() const {
    return new DepositoNormal(*this);
}

template<class T>
bool DepositoNormal<T>::inserirProdutos(const vector<T *>& vprd) {
    int vi = 0;
    
    for (int i = 0; i < deposito.size(); ) {
        while(!deposito[i].isFull() && vi < vprd.size()) {
            deposito[i].push(*vprd[vi]);
            vi++;
        }
        i+=2;
    }
    
    if(vi < vprd.size()) {
        for (int i = 1; i < deposito.size(); ) {
            while(!deposito[i].isFull() && vi < vprd.size()) {
                deposito[i].push(*vprd[vi]);
                vi++;
            }
            i+=2;
        }
    }
    
    return (vi == vprd.size() ? true : false);
}

template<class T>
vector<T> DepositoNormal<T>::expedirProdutos(int qtd) {
    int c=0;
    vector<T> exp;
    
    for (int i = 1; i < deposito.size();) {
        while(c < qtd && deposito[i].size() > 0) {
            exp.push_back(deposito[i].top());
            deposito[i].pop();
            c++;
        }
        i+=2;
    }
    
    if(c < qtd) {
        for (int i = 0; i < deposito.size();) {
            while(c < qtd && deposito[i].size() > 0) {
                exp.push_back(deposito[i].top());
                deposito[i].pop();
                c++;
            }
            i+=2;
        }
    }
    
    return exp;
}

template<class T>
bool DepositoNormal<T>::isFull() const {
    int flag = 0;
    
    for (int i = 0; i < deposito.size(); i++) {
        if(deposito[i].isFull()) {
            flag++;
        }
    }
    
    return ((flag == deposito.size()) ? true : false);
}

template<class T>
vector<PaleteNormal<T>> DepositoNormal<T>::getDeposito() const {
    return deposito;
}

template<class T>
void DepositoNormal<T>::setDeposito(vector<PaleteNormal<T>> vpn) {
    deposito = vpn;
}

template<class T>
bool DepositoNormal<T>::operator ==(const DepositoNormal<T>& dn) {
    if(Deposito<T>::operator ==(dn)) {
        if(this->deposito.size() == dn.deposito.size()) {
            for (int i = 0; i < deposito.size(); i++) {
                if(!(this->deposito[i] == dn.deposito[i])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    return false;
}

template<class T>
DepositoNormal<T>& DepositoNormal<T>::operator=(const DepositoNormal<T>& dn) {
    if(this != &dn) {
        Deposito<T>::operator =(dn);
        this->deposito = dn.deposito; //validar isto
    }
    
    return *this;
}

template<class T>
void DepositoNormal<T>::escreve(ostream& out) const {
    out << "Depósito Normal";
    Deposito<T>::escreve(out);
    for (int i = 0; i < deposito.size(); i++) {
        out << "Palete Nº " << (i+1) << ": ";
        deposito[i].escreve(out);
    }
    out << endl;
}

template<class T>
void DepositoNormal<T>::writeFile(ostream& out) const {
    Deposito<T>::writeFile(out);
    out << ";Normal" << endl;
    for (int i = 0; i < deposito.size(); i++) {
        deposito[i].writeFile(out);
    }
    out << endl;
}

template<class T>
ostream& operator <<(ostream& out, const DepositoNormal<T>& dn) {
    dn.escreve(out);
    return out;
}

#endif	/* DEPOSITONORMAL_H */