/*  
 * File:   ArmazemOld.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:46
 */

#ifndef ArmazemOld_H
#define	ArmazemOld_H

#include <list>
#include "Deposito.h"
#include "DepositoNormal.h"
#include "DepositoFresco.h"

template<class T>
class ArmazemOld{
    private:
        int ID;
        list<Deposito<T> *> depositos;
        double** distDepositos;
        
    public:
        ArmazemOld(int ID, int ndf, int ndn);
        ArmazemOld(const ArmazemOld<T>& arm);
        ArmazemOld();
        ~ArmazemOld();
        
        int getID() const;
        void setID(int id);
        
        bool inserirProdutos(const vector<T *>& prds);
        vector<T> expedirProdutos(int id, int nrProdutos);
        
        double** getDistDepositos() const;
        
        void adicionarDepositoFresco(int cap, int lim, float area);
        void adicionarDepositoNormal(int cap, int lim, float area);
        
        list<Deposito<T> *> getDepositos() const;
        void setDeposito( const list<Deposito<T> &> list);
        
        void createFile(string path);
        
        void escreve(ostream& out) const;
        void writeFile(ostream& out) const;
        void escreveDistanciaDepositos(ostream& out) const;
};


template<class T>
ArmazemOld<T>::ArmazemOld(int i, int ndf, int ndn) {
    ID = i;
    int nd = ndf+ndn;
    
    distDepositos = new double*[nd];
    
    for (int i = 0; i < nd; ++i) {
        distDepositos[i] = new double[nd];
    }
}

template<class T>
ArmazemOld<T>::ArmazemOld() {
    ID = 0;
}

template<class T>
ArmazemOld<T>::ArmazemOld(const ArmazemOld<T>& arm) {
    ID = arm.ID;
    depositos(arm.depositos);
    distDepositos = arm.distDepositos;
}

template<class T>
ArmazemOld<T>:: ~ArmazemOld(){
    
}

template<class T>
bool ArmazemOld<T>::inserirProdutos(const vector<T *>& prds) {
    typename vector<T *>::const_iterator it;
    typename list<Deposito<T> *>::iterator lit;
    vector<Produto *> prdf;
    vector<Produto *> prdn;
    Produto * p;
    
    for(it = prds.begin(); it != prds.end(); it++) {
        p = dynamic_cast<Produto *>(*it);
        if(typeid(*p) == typeid(ProdutoFresco)) {
            prdf.push_back(p);
        } else if(typeid(*p) == typeid(ProdutoNormal)) {
            prdn.push_back(p);
        }
    }
    
    Deposito<T>* d;
    int nf = 0, isf=0, isn=0, nn = 0;
    for(lit = depositos.begin(); lit != depositos.end(); lit++) {
        d = dynamic_cast<Deposito<T> *>(*lit);
        if (typeid(*d) == typeid(DepositoFresco<T>)) {
            if(!(*d).isFull() && isf == 0) {
                if((*d).inserirProdutos(prdf)) {
                    nf = (int) prdf.size();
                }
                isf = 1;
            }
        } 
        if (typeid(*d) == typeid(DepositoNormal<T>)) {
            if(!(*d).isFull() && nn == 0) {
                 if((*d).inserirProdutos(prdn)) {
                     nn = (int) prdn.size();
                 }
                 isn = 1;
            }
        }
    }
    
    return (nn+nf == (prdn.size() + prdf.size()) ? true : false);
}

template<class T>
vector<T> ArmazemOld<T>::expedirProdutos(int id, int nrProdutos) {
    typename list<Deposito<T> *>::iterator it;
    Deposito<T> * d;
    for(it = depositos.begin(); it != depositos.end(); it++) {
        d = dynamic_cast<Deposito<T> *>(*it);
        if(d->getIDDeposito() == id) {
            return d->expedirProdutos(nrProdutos);
        }
    }
    
    return vector<T>();
}

template<class T>
int ArmazemOld<T>:: getID() const{
    return ID;
}

template<class T>
void ArmazemOld<T>:: setID(int ID){
    this->ID = ID;
}

template<class T>
list<Deposito<T> *> ArmazemOld<T>:: getDepositos() const{
    return this->depositos;
}

template<class T>
void ArmazemOld<T>::setDeposito(const list<Deposito<T>&> list) {
    depositos(list);
}

template<class T>
double** ArmazemOld<T>::getDistDepositos() const {
    return distDepositos;
}

template<class T>
void ArmazemOld<T>::adicionarDepositoFresco(int cap, int lim, float area) {
    DepositoFresco<T> d((int) depositos.size(), cap, lim, area);
    depositos.push_back(d.clone());
    
}

template<class T>
void ArmazemOld<T>::adicionarDepositoNormal(int cap, int lim, float area) {
    DepositoNormal<T> d2((int) depositos.size(), cap, lim, area);
    depositos.push_back(d2.clone());
    
}
        

template<class T>
void ArmazemOld<T>:: createFile(string path){
    ofstream file;
    
    file.open(path);
    this->writeFile(file);
    file.close();
}

template<class T>
void ArmazemOld<T>::writeFile(ostream& out) const {
    out << ID << endl; // ID do Depósito
    out << depositos.size() << ";" << depositos.size() << endl; // Tamanho da Matriz
    this->escreveDistanciaDepositos(out);
    
    typename list<Deposito<T> *>::const_iterator it;
    Deposito<T>* d;
    for(it = depositos.begin(); it != depositos.end(); ++it) {
        d = dynamic_cast<Deposito<T> *>(*it);
        (*d).writeFile(out);
    }
}

template<class T>
void ArmazemOld<T>::escreve(ostream& out) const {
    out << "Armazém Nº: " << ID << endl;
    out << "\nDistâncias de Depósitos: " << endl;
    this->escreveDistanciaDepositos(out);
    
    out << "\nConteúdo Depósitos:" << endl;
    
    typename list<Deposito<T> *>::const_iterator it;
    Deposito<T>* d;
    for(it = depositos.begin(); it != depositos.end(); ++it) {
        d = dynamic_cast<Deposito<T> *>(*it);
        out << *d << endl;
    }
}

template<class T>
void ArmazemOld<T>::escreveDistanciaDepositos(ostream& out) const {
    for (int i = 0; i < depositos.size(); i++) {
        for (int j = 0; j < depositos.size(); j++) {
            out << distDepositos[i][j] << ";";
        }
        out << endl;
    }
    out << endl;
}

template<class T>
ostream& operator<<(ostream& out, const ArmazemOld<T>& arm) {
    arm.escreve(out);
    return out;
}


#endif	/* ArmazemOld_H */

