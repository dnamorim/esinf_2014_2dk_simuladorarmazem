/* 
 * File:   Palete.h
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:47
 */

#ifndef PALETENORMAL_H
#define	PALETENORMAL_H

template<class T>
class PaleteNormal {

    private :
        stack<T> palete;
        int capacidade;
        
    public :
        PaleteNormal(int c);
        PaleteNormal(const PaleteNormal<T>& pn);
        PaleteNormal();
        ~PaleteNormal();
        
        PaleteNormal<T>* clone() const;
        
        bool push(const T& p);
        T top();
        void pop();
        int size();
        
        int getCapacidade() const;
        bool isFull() const;
        
        bool operator==(const PaleteNormal<T>& pn);
        PaleteNormal<T>& operator=(const PaleteNormal<T>& pn);
        
        void escreve(ostream& out) const;
        void writeFile(ostream& out) const;
};

template<class T>
PaleteNormal<T>::PaleteNormal(int c) : palete() {
    capacidade = c;
}

template<class T>
PaleteNormal<T>::PaleteNormal(const PaleteNormal& pn) : palete(pn.palete) {
    capacidade = pn.capacidade;
}

template<class T>
PaleteNormal<T>::PaleteNormal() {
    palete();
    capacidade = 0;
}

template<class T>
PaleteNormal<T>::~PaleteNormal() {
    
}

template<class T>
bool PaleteNormal<T>::isFull() const {
    if(palete.size() == capacidade) {
        return true;
    } else {
        return false;
    }
}

template<class T>
int PaleteNormal<T>::getCapacidade() const {
    return capacidade;
}

template<class T>
int PaleteNormal<T>::size() {
    return (int) palete.size();
}

template<class T>
bool PaleteNormal<T>::push(const T& p) {
    if(palete.size() < capacidade) {
        palete.push(p);
        return true;
    }
    
    return false;
}

template<class T>
T PaleteNormal<T>::top() {
    return palete.top();
}

template<class T>
void PaleteNormal<T>::pop() {
    if(!palete.empty()) {
        palete.pop();
    }
}

template<class T>
bool PaleteNormal<T>::operator ==(const PaleteNormal<T>& pn) {
    //se o tamanho das paletes forem diferentes
    if((int) palete.size() == (int) pn.palete.size()) {
        stack<T> stk1 = palete;
        stack<T> stk2 = pn.palete;
        
        while(!stk1.empty()) {
            T t1 = stk1.top();
            T t2 = stk2.top();
            
            //se os objectos actuais da Paletes forem diferentes
            if(!(t1 == t2)) {
                return false;
            }
            
            stk1.pop();
            stk2.pop();
        }
        
        return true;
    }
    
    return false;
}

template<class T>
PaleteNormal<T>& PaleteNormal<T>::operator =(const PaleteNormal<T>& pn) {
    if(this != &pn) {
        stack<T> stkEx = pn.palete;
        palete = stkEx;
        capacidade = pn.capacidade;
    }
    return *this;
}

template<class T>
PaleteNormal<T>* PaleteNormal<T>::clone() const {
    return new PaleteNormal(*this);
}

template<class T>
void PaleteNormal<T>::escreve(ostream& out) const{
    stack<T> stk = palete;
    while(!stk.empty()) {
        out << stk.top() << "; ";
        stk.pop();
    }
    out << endl;
}

template<class T>
void PaleteNormal<T>::writeFile(ostream& out) const{
    stack<T> stk = palete;
    while(!stk.empty()) {
        stk.top().writeFile(out);
        out << ";";
        stk.pop();
    }
    out << endl;
}

template <typename T>
ostream& operator << (ostream& out, const PaleteNormal<T>& pn) {
    pn.escreve(out);
    return out;
}

#endif	/* PALETENORMAL_H */

