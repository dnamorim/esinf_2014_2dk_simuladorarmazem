/* 
 * File:   main.cpp
 * Author: dnamorim
 *
 * Created on 16 de Outubro de 2014, 11:41
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <list>

using namespace std;

#include "Produto.h"
#include "ProdutoNormal.h"
#include "ProdutoFresco.h"
#include "ArmazemOld.h"
#include "Armazem.h"
#include "GeradorOld.h"

/*
 * 
 */

void mainVersao1() {
    int min, max;
    cout << "Valor Mínimo: ";
    cin >> min;
    cout << "Valor Máximo: ";
    cin >> max;
    
    cout << endl;
    GeradorOld<Produto> ger(min, max);
    
    
    vector<Produto *> vp;
    vp.push_back(ProdutoFresco("Apple").clone());
    vp.push_back(ProdutoFresco("HP").clone());
    vp.push_back(ProdutoFresco("Dell").clone());
    vp.push_back(ProdutoFresco("Alienware").clone());
    vp.push_back(ProdutoFresco("Asus").clone());
    vp.push_back(ProdutoFresco("Toshiba").clone());
    vp.push_back(ProdutoFresco("MSI").clone());
    vp.push_back(ProdutoFresco("Intel").clone());
    vp.push_back(ProdutoFresco("Samsung").clone());
    vp.push_back(ProdutoFresco("Packard Bell").clone());
    vp.push_back(ProdutoFresco("Sony").clone());
    vp.push_back(ProdutoFresco("Acer").clone());
    vp.push_back(ProdutoFresco("Compaq").clone());
    
    vp.push_back(ProdutoNormal("Audi").clone());
    vp.push_back(ProdutoNormal("BMW").clone());
    vp.push_back(ProdutoNormal("Volvo").clone());
    vp.push_back(ProdutoNormal("Toyota").clone());
    vp.push_back(ProdutoNormal("Honda").clone());
    vp.push_back(ProdutoNormal("Aston Martin").clone());
    vp.push_back(ProdutoNormal("Opel").clone());
    vp.push_back(ProdutoNormal("Porsche").clone());
    vp.push_back(ProdutoNormal("Rover").clone());
    vp.push_back(ProdutoNormal("Volkswagen").clone());
    vp.push_back(ProdutoNormal("Nissan").clone());
    vp.push_back(ProdutoNormal("Renault").clone());
    vp.push_back(ProdutoNormal("Citroen").clone());
    
    cout << "Armazém Gerado: " << endl;
    ger.mostraArmazemOld();
    
    cout << "A Inserir Produtos..." << endl;
    ger.inserirProdutos(vp);
    
    cout << "Armazém com Produtos Adicionados: " << endl;
    ger.mostraArmazemOld();
    
    cout << "Localização para gravar o ficheiro Armazém (txt): ";
    string path;
    cin >> path;
    ger.gravarArmazemOld(path);
    
    cout << "Expedição de Produtos: " << endl;
    ger.expedirProdutos();
    
    cout << "Armazém após Expedição: " << endl;
    ger.mostraArmazemOld();
}

void showMenu() {
    cout << "ESINF Simulador de Armazém\n" << endl;
    cout << "1. Correr Gerador de Armazém" << endl;
    cout << "2. Ler Armazém de Ficheiro para Grafo" << endl;
    cout << "3. Apresentar todos os Percursos Possíveis entre 2 depósitos" << endl;
    cout << "4. Apresentar Percurso com o mesmo Tipo de Depósito" << endl;
    cout << "5. Calcular o Percurso Mais Curto entre 2 Depósitos" << endl;
    cout << "0. Terminar Simulador" << endl;
    cout << endl;
    cout << "Insira a opção: ";
}

void errorNotImported() {
    cout << "Erro: Não pode executar esta opção. Por favor, importe um Armazém para Grafo." << endl;
}

int main(int argc, char** argv) {
    int opt;
    Armazem a;
    bool imported = false;
    
    do {
        showMenu();
        cin >> opt;
        cin.get();

        switch (opt) {
            case 1:
            {
                mainVersao1();
                break;
            }
            case 2:
            {
                cout << "Localização do ficheiro do Armazém: ";
                string path;
                cin >> path;
                a.readFile(path);
                imported = true;
                cout << endl;
                break;
            }
            case 3:
            {
                if(imported) {
                    int idO, idD;
                    cout << "ID Depósito de Origem: ";
                    cin >> idO;
                    cin.get();
                    cout << "ID Depósito de Destino: ";
                    cin >> idD;
                    cin.get();
                    cout << endl;
                    a.percursosPossiveisDepositos(idO, idD);
                } else {
                    errorNotImported();
                }
                break;
            }
            case 4:
            {
                if(imported) {
                    int idO, idD;
                    cout << "ID Depósito de Origem: ";
                    cin >> idO;
                    cin.get();
                    cout << "ID Depósito de Destino: ";
                    cin >> idD;
                    cin.get();
                    cout << endl;
                    a.percursoTipo(idO, idD);
                } else {
                    errorNotImported();
                }
                break;
            }
            case 5:
            {
                if(imported) {
                    int idO, idD;
                    cout << "ID Depósito de Origem: ";
                    cin >> idO;
                    cin.get();
                    cout << "ID Depósito de Destino: ";
                    cin >> idD;
                    cin.get();
                    cout << endl;
                    a.menorPercursoDepositos(idO, idD);
                } else {
                    errorNotImported();
                }

                break;
            }
            case 0:
                cout << "A Aplicação vai terminar..." << endl;
                break;
            default:
                cout << "Erro: Inseriu uma opção Inválida." << endl;
                break;
        }
        cout << endl;
    } while(opt != 0);


    return 0;
}

