/* 
 * File:   ProdutoFresco.h
 * Author: Ricardo Rodrigues
 *
 * Created on 22 de Outubro de 2014, 22:39
 */

#ifndef PRODUTOFRESCO_H
#define	PRODUTOFRESCO_H

#include "ProdutoNormal.h"


class ProdutoFresco : public Produto
{
    public :
        ProdutoFresco(const string nome);
        ProdutoFresco();
        ProdutoFresco(const ProdutoFresco& p);
        ~ProdutoFresco();
        virtual Produto* clone() const;
        
        virtual bool operator==(const ProdutoFresco& pf);
        virtual ProdutoFresco& operator=(const ProdutoFresco& pf);
        
        void escreve(ostream& out) const;
};

ProdutoFresco::ProdutoFresco(const string nome) : Produto(nome) {
    
}

ProdutoFresco::ProdutoFresco(const ProdutoFresco& pf) : Produto(pf) {
    
}

ProdutoFresco::~ProdutoFresco() {
    
}

Produto* ProdutoFresco::clone() const {
    return new ProdutoFresco(*this);
}

bool ProdutoFresco::operator ==(const ProdutoFresco& pf) {
    if (typeid(pf) == typeid(ProdutoFresco)) {
        return Produto::operator ==(pf);
    }
    
    return false;
}

ProdutoFresco& ProdutoFresco::operator =(const ProdutoFresco& pf) {
    if(this != &pf) {
        Produto::operator =(pf);
    }
    
    return *this;
}

void ProdutoFresco::escreve(ostream& out) const {
    Produto::escreve(out);
    out << "(Tipo: Produto Fresco)" << endl;
}
        
ostream& operator<<(ostream& out, const ProdutoFresco& pf) {
    pf.escreve(out);
    return out;
}

#endif	/* PRODUTONORMAL_H */

